$(function () {

    $('.toggler').on('click', function (e) {
        var target = $(this).data('target'),
            className = $(this).data('class');

        if (className) $(target).addClass('is-active');
        else {
            $(target).slideToggle(400);
        }

        $('.scrollPane').data('jsp').reinitialise();
        $('.scrollPane').data('jsp').scrollTo(0,0);
        e.stopPropagation();
    });

});
