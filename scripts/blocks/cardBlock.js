$(function () {

    $(document).on('click touchstart', function (e) {
        if ((!$(e.target).closest(".cardBlock").length)) {
            $('.cardBlock').removeClass('is-active');
        }
    });


    $('.card').on('click', function () {
        var activeCard = $(this);
        if (activeCard.parents('.cardBlock').length < 1) return; //only for cards in cardBlock
        activeCard.addClass('is-active')
            .siblings().removeClass('is-active')
            .parents('.cardBlock').removeClass('is-active');
        activeCard.siblings().eq(0).before(this);
        activeCard.parents('.scrollPane').data('jsp').positionDragY(0);

        setParams();
        function setParams() {
            var name = activeCard.find('.card-name').text(),
                desc = activeCard.find('.card-desc').text(),
                amount = activeCard.find('.card-amount').html(),
                status = activeCard.hasClass('card--yellow') ? 'card--yellow' : 'card--green';

            var label = activeCard.parents('.cardBlock').siblings('.card');
            label.find('.card-name').text(name);
            label.find('.card-desc').text(desc);
            label.find('.card-amount').html(amount);
            label.removeClass('card--green card--yellow').addClass(status)
        }
    })

});
