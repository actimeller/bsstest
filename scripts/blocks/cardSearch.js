$(function () {

    $('.cardSearch-clear').on('click', function () {
        $(this).siblings('.cardSearch-input').val('')
    });

    $('.cardSearch-input').on('focus', function () {
        $(this).parent().addClass('is-focus')
    }).on('blur', function () {
        $(this).parent().removeClass('is-focus')
    })

});
